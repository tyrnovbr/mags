	var valueSlider = null;
	var flagSlider;
	var slider1 = null;
	var slider2 = null;

	function loadSlider1(value) {

		if($('.bxslider').length) {

	    	slider1 = $('.bxslider').bxSlider({
	  				slideMargin:20,
	  				slideWidth: 226,
	  				minSlides: value || 5,
	  				maxSlides: value || 5,
	  				moveSlides: 1,
	  				pager: false,
			        nextText: '<i class="fa fa-angle-right"></i>',
	        		prevText: '<i class="fa fa-angle-left"></i>'
	    	});
		}
	}

	function loadSlider2(value) {

	    	if($('#slider2').length) {

	    	slider2 = $('#slider2').bxSlider({
	  				slideMargin:25,
	  				slideWidth: 290,
	  				minSlides: value || 4,
	  				maxSlides: value || 4,
	  				moveSlides: 1,
	  				pager: false,
			        nextText: '<i class="fa fa-angle-right"></i>',
	        		prevText: '<i class="fa fa-angle-left"></i>'
	    	});

		}
	}


$(document).ready(function() {

	// search button

	/*	$('#header__search-button').on('click', function(e) {
			$('#header__search-field').toggleClass('header-top__search-open');

			if($('#header__search-field').hasClass('header-top__search-open')) {
				$('#header__search-field').focus();
			}

			return false;	
		}); */

	// scroll top button

		$('#footer__scroll-top').on('click', function(e) {
			$(window).scrollTo(0, 300);

			return false;
		});

	// validation form

		var validateText = 'Это поле обязательно для заполнения';

		var validateObjectSettings = {
			rules: {
				name: 'required',
				phone: {
	      			required: true,
	      			minlength: 16
	    		},
	    		fio:'required',
	    		email: {
	    			required:true,
	    			email:true
	    		},
	    		text:{
	      			required: true,
	      			minlength: 16	    			
	    		}
			},
			messages: {
				name: validateText,
				fio: validateText,
				phone: validateText,
				email: validateText,
				text:validateText
			},
			errorClass: "error-validate",
			errorElement: "span"
		}

		$('#form-validate1').validate(validateObjectSettings);
		$('#form-validate2').validate(validateObjectSettings);
		$('#form-validate3').validate(validateObjectSettings);
		$('#form-validate4').validate(validateObjectSettings);

	// phone mask

		$('.phone-mask').mask('+7(000)000-00-00');


	// yandex map       
        if($('#map').length) {

	        ymaps.ready(init);

	        var myMap, 
	            myPlacemark;

	        }

          function init(){ 

              myMap = new ymaps.Map("map", {
                  center: [51.6735,39.1685],
                  zoom: 10,
                  controls: []
              });
              
              myPlacemark = new ymaps.Placemark([51.6735,39.1685], {}, {
                iconLayout: 'default#image',
                  iconImageHref: '/sites/pk-mags/pk-mags/images/map-icon.png',
                  iconImageSize: [155, 81], 
                  iconImageOffset: [-78, -80] 
              });

              
              myMap.geoObjects.add(myPlacemark);

              myMap.controls.add('zoomControl');

        	  myMap.behaviors.disable('scrollZoom');

          }

    // adaptive

    	function getArrowsTextRemove(size) {


    		if(size == 'small') {
    			$('#news_prev').text('предыдущая');
      			$('#news_next').text('следующая');

      			return;
    		}

    			$('#news_prev').text('предыдущая новость');
      			$('#news_next').text('следующая новость');

    	}

    	var resize = null;
    	var wrap = null;

	    (resize = function() {

	    	var windowWidth = $(window).width();

	    	if(windowWidth < 1200 && windowWidth > 992) {

	    		$('#left-cat-wrap').append($('#left-category'));

	    		$('#diller-container').append($('#diller'));
	    		$('#manufacturer-container').append($('#manufacturer'));
	    		$('#product-md-cont').append($('#product-center-wrap'));

	    		$('#product__main-cont').append($('#product__title-main'));


	    		$('#right-wrap').append($('#call-back-btn'));
	    		$('#city-container').append($('#city'));

	    		$('#bg-sm').height(0);

	    		// Возвращаем блок каталог на место

	    		$('#catalog-container-block').append($('#catalog__container-right'));


		    	if($('.news__nav-arrow').length) {
		    		getArrowsTextRemove();
		    	}

		    	if(slider1 !== null && flagSlider !== 'bigscreen') {

		    		slider1.destroySlider();
		    		setTimeout(function() {

	    				$('.bxslider').removeAttr('style');
	    				loadSlider1();	    // load new slider

    				}, 1);

		    	}

		    	if(slider2 !== null && flagSlider !== 'bigscreen') {
		    		slider2.destroySlider();

		    		setTimeout(function() {

		    			$('#slider2').removeAttr('style');

		    			loadSlider2(); // load new slider

		    		}, 1);

		    	}

		    	flagSlider = 'bigscreen';

	    	} else if(windowWidth >= 1200) {


	    		$('#left-cat-wrap').append($('#left-category'));

	    		$('#product__main-cont').append($('#product__title-main'));
	    		$('#product-md-cont').append($('#product-center-wrap'));


	    		$('#call-back-wrap').append($('#call-back-btn'));
	    		$('#city').insertBefore($('#phone-main'));

	    		$('#diller-container').append($('#diller'));
	    		$('#manufacturer-container').append($('#manufacturer'));

	    		$('#bg-sm').height(0);

	    		// Возвращаем блок каталог на место

	    		$('#catalog-container-block').append($('#catalog__container-right'));

		    	if($('.news__nav-arrow').length) {
		    		getArrowsTextRemove();
		    	}




		    	if(slider1 !== null && flagSlider !== 'bigscreen') {

		    		slider1.destroySlider();

		    		setTimeout(function() {

	    				$('.bxslider').removeAttr('style');
	    				loadSlider1();	    // load new slider

    				}, 2);

		    	}


		    	if(slider1 !== null && flagSlider !== 'bigscreen') {

		    		slider1.destroySlider();
		    		setTimeout(function() {

	    				$('.bxslider').removeAttr('style');
	    				// loadSlider1();	    // load new slider

    				}, 1);

		    	}


		    	if(slider2 !== null && flagSlider !== 'bigscreen') {

		    		slider2.destroySlider();
		    		setTimeout(function() {

	    				$('#slider2').removeAttr('style');
	    				loadSlider2();	    // load new slider

    				}, 1);


		    	}

		    	flagSlider = 'bigscreen';


	    	} else if(windowWidth < 992 && windowWidth > 768) {


	    		$('#modal__category .modal-content').append($('#left-category'));

	    		$('#product__title-container').append($('#product__title-main'));
	    		$('#sm-cont-wrap').append($('#product-center-wrap'));


	    		$('#city-container').append($('#city'));
	    		$('#menu-container').append($('.header-bottom__link-wrap'));
	    		$('#call-back-wrap').append($('#call-back-btn'));


	    		// Счет высоты бг

	    		var bgHeight = $('.banner-form__wrap').height();
	    		$('#bg-sm').height(bgHeight);

	    		// Перестановка блока каталога

	    		$('#catalog__sm-container').append($('#catalog__container-right'));

	    		setTimeout(function() {

	    			$('.slicknav_nav').html(wrap);

	    			$('.header-nav-list__level2 > li > .slicknav_row a').unwrap();	    			
	    		},1);

			    	if($('.news__nav-arrow').length) {
			    		getArrowsTextRemove();
			    	}


		    	if(slider1 !== null && flagSlider !== 'bigscreen') {

		    		slider1.destroySlider();
		    		setTimeout(function() {

	    				$('.bxslider').removeAttr('style');
	    				loadSlider1();	    // load new slider

    				}, 1);

		    	}

		    	if(slider2 !== null && flagSlider !== 'bigscreen') {
		    		slider2.destroySlider();

		    		setTimeout(function() {

		    			$('#slider2').removeAttr('style');

		    			loadSlider2(); // load new slider	

		    		}, 1);

		    	}


		    	flagSlider = 'bigscreen';

	    	} else if(windowWidth < 768) {

	    		$('#modal__category .modal-content').append($('#left-category'));

				$('#product__title-container').append($('#product__title-main'));

	    		setTimeout(function() {
	    			$('.header-nav-list__dropdown-menu > .slicknav_row a').unwrap();	    			
	    		},1);


	    		$('#city-container').append($('#city'));
	    		$('#menu-container').append($('.header-bottom__link-wrap'));
	    		$('#call-back-wrap').append($('#call-back-btn'));


	    		// Счет высоты бг

	    		var bgHeight = $('.banner-form__wrap').height();
	    		$('#bg-sm').height(bgHeight);

	    		// Перестановка блока каталога

	    		$('#catalog__sm-container').append($('#catalog__container-right'));

		    	if($('.news__nav-arrow').length) {
		    		getArrowsTextRemove('small');
		    	}



		    	valueSlider = 1;


		    	if(slider1 !== null && flagSlider !== 'smallscreen') {

		    		slider1.destroySlider();
		    		setTimeout(function() {

	    				$('.bxslider').removeAttr('style');
	    				loadSlider1(valueSlider);	    // load new slider

    				}, 1);

		    	}

		    	if(slider2 !== null && flagSlider !== 'smallscreen') {
		    		slider2.destroySlider();

		    		setTimeout(function() {

		    			$('#slider2').removeAttr('style');

		    			loadSlider2(valueSlider); // load new slider	
		    				    			
		    		}, 1);

		    	}


		    	flagSlider = 'smallscreen';

	    	}


	    })();


	    $(window).on('resize', resize);


	    // mobile menu


	    $('#menu').slicknav({
	    	parentTag: 'span',
	    	duration: 0,
	    	closedSymbol: '<i class="fa fa-angle-right symbol-blue"></i>',
	    	openedSymbol: '<i class="fa fa-angle-right"></i>',
	    	init: function() {

	    		$('#mobile-menu').on('click', function() {
	    			$('#menu').slicknav('open');
	    		});

	    		wrap = $('.slicknav_nav').html();

	    		$('.slicknav_btn').before('<i id="close-mobile-menu" class="fa fa-times"></i>');

	    		$('#close-mobile-menu').on('click', function() {
	    			$('#menu').slicknav('close');

	    			$('body').removeClass('modal-open');
	    			$('.slicknav_menu').removeClass('max-height');
	    			$('#close-mobile-menu').removeClass('close-active');
	    		});
	    	},
	    	beforeOpen: function() {
	    		$('.slicknav_menu').addClass('max-height');
	    		$('body').addClass('modal-open');

	    		$('#close-mobile-menu').addClass('close-active');
	    	}
	    });

	    // select

	    if($('select').length) {

		    $('select').select2();
	    	$('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

			    if($('.product-main-select').length) {
			    	$('.select2').addClass('select-product-page');
			    }

	    }

    	// aside filter menu

    	$('.catalog-list__left-block > li').on('click', function() {

    		var elem = $(this);

	    		$('.catalog-list__left-block > li').removeClass('catalog-list__left-block-active');

	    		elem.addClass('catalog-list__left-block-active');

	    		elem.find('.catalog-list__level2').slideToggle(function() {

			       if ($(this).parent().is(':hidden')) {
			       		elem.removeClass('catalog-list__left-block-active');

			        	
			       }

	    		});

	    		$(this).siblings().children().next().slideUp();

    		

    	});
    
    
    
    $('.js-button-add-basket').click(function(){
        var id=$(this).attr('id');
        var value=$('.js-value-number-'+id).val();
        var valueElem = document.getElementById('quantity_for_bask');
        $(valueElem).html(value +'шт.');
        var summElem = document.getElementById('summ_for_bask');
        var price = parseFloat($(summElem).attr('price'));
        $(summElem).html(price*value +'р.');
        if(value!=0){
            
            $.ajax({
                type     :'POST', 
                cache    : false,
                data: $('.js-form-add-basket-'+id).serialize(),
                url  : '/cart_items',
                success  : function(response) { 
                }
            });
        }
    });
    
    $('.js-button-minus').click(function(){
        var id=$(this).attr('id');
        var value=parseFloat($('.js-value-number-'+id).val());
        var price=parseFloat($('.price-for-'+id).attr('price'));
        var type=$(this).attr('type');
        if(value!=0){ 
            value--;
            $('.js-value-number-'+id).val(value);
            var sum_product=Math.round(value*price * 100) / 100 ;
            $('.js-sum-tovar-'+id).html(sum_product+'<span>р.</span>');
        }
        if(type=='basket'){
            update_basket(id);
            
        }
        
    });
    $('.js-button-plus').click(function(){
        var id=$(this).attr('id');
        var value=parseFloat($('.js-value-number-'+id).val());
        var price=parseFloat($('.price-for-'+id).attr('price'));
        value++;
        $('.js-value-number-'+id).val(value);
        var sum_product=Math.round(value*price * 100) / 100 ;
        $('.js-sum-tovar-'+id).html(sum_product+'<span>р.</span>');
        var type=$(this).attr('type');
        if(type=='basket'){
            update_basket(id);
        }
    });
     
    
     function update_basket(id){
        $.ajax({
            type     :'PUT', 
            cache    : false,
            data: $('.js-form-update-basket-'+id).serialize(),
            url  : '/cart_items/' +id,
            success  : function(response) { 
                
                
            }
            
        }); 
    }


});


$(window).load(function(){

	// masonry

		if($('#grid-item').length) {

		    $( '#grid-item' ).masonry({
		        itemSelector: '.grid-item',
		        columnWidth: '.grid-item'
		    });
		}

		loadSlider1(valueSlider);
		loadSlider2(valueSlider);

	    // img modal


		$('#product__link-img').on('click', function() {
			$('.modal__imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#modal__img-cont').modal('show');  

			return false; 
		});

		// category modal

		(function() {

			var elem = $('#catalog-list__main-title').clone().text();

			$('#modal__cont-title').append('<div class="modal__title-main">'+ elem +'</div>')

			$('#category-sm-cont a').on('click', function() {
				$('#modal__category').modal('show');  

				return false;
			});

		}());


});


